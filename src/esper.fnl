#!/usr/bin/fennel
;;Author: Will Sinatra <wpsinatra@gmail.com>
;; GPLv3 | v0.1
;;=====[ Libraries ]=====
(local lume (require "lume"))
(local http (require "socket.http"))
(local https (require "ssl.https"))

;;=====[ Global ]=====
(var bindir "~/bin/")
(var builddir "")
(var srcdir "")

;;=====[ Tables ]=====
(var espertbl {})

;;Replace this with luasocket/luasec calls
;;(fetch "https://..." "something.tar.gz")
(fn fetch [tbl]
  (var warg "")
  (let
      [outf (. tbl "outf")
       at (. tbl "at")
       git (. tbl "git")
       url (. tbl "url")
       extract (. tbl "extract")
       typ (. tbl "atype")]
    (if
     (and (= url nil) (= at nil))
     (do
       (print "fetch requires a url or at arg!")
       (os.exit)))
    (if
     (~= url nil)
     (do
       (if (or (= git "") (or (= git nil)))
           (if (or (= outf "")) (or (= outf nil))
               (set warg (.. "wget -O " url)))
           (= git true)
           (set warg (.. "git clone " url))
           (set warg (.. "wget " url " -O " outf)))
       (os.execute (.. "cd " srcdir " && " warg))))
    (if
     (~= at nil)
     (do
       (if (= extract true)
           (do
             (os.execute (.. "cp " at " " srcdir "/" outf)))
           (os.execute (.. "cp -r " at " " srcdir)))))
    (if (= extract true)
        (if (= typ "gzip")
            (os.execute (.. "cd " srcdir " && tar -xvzf " outf))
            (= typ "bzip2")
            (os.execute (.. "cd " srcdir " && tar -xvjf " outf))
            ( = typ "zip")
            (os.execute (.. "cd " srcdir " && unzip " outf))))))

(fn prepare []
  (let
      [pw (io.popen "pwd")
       pw (pw:read "*a")
       pw (string.gsub pw "[\r\n]" "")
       src (.. pw "/espersrc")]
    (set srcdir src)
    (os.execute (.. "mkdir " src)))
  (set builddir (.. srcdir (. espertbl "builddir"))))

(fn cleanup []
  (if (= (. espertbl "debug") true)
      nil
      (os.execute (.. "rm -rf " srcdir))))

(fn ensure [tbl]
  (each [k v (pairs tbl)]
    (do
      (os.execute (.. "mkdir -p " v)))))

;; (depends {alpine="make autoconf fennel", debian="make autoconf"})
(fn depends [tbl]
  (if (= tbl {})
      (print "No depends")
      (do
        (let
            [distro (io.popen "grep ^ID= /etc/os-release | awk -F'=' '{print $2}'")
             distro (distro:read "*a")
             distro (string.gsub distro "[\r\n]" "")]
          (if
           (= (. tbl distro) nil)
           (print "No depends for target system")
           (if
            (or (= distro "alpine") (= distro "postmarketos"))
            (os.execute (.."apk -U add " (. tbl "alpine")))
            (or (= distro "debian") (= distro "ubuntu"))
            (os.execute (.. "DEBIAN_FRONTEND=noninteractive apt-get install -y " (. tbl "debian")))
            (or (= distro "fedora") (= distro "centos") (= distro "rhel"))
            (os.execute (.. "yum install -y " (. tbl "rhel")))
            (print "Valid OS types: alpine, debian, rhel")))))))

;;distro=$(grep "^ID=" /etc/os-release | awk -F'=' '{print $2}')
;; Add something like this ^ so that you can have an alpine= under build and provide specific args to build, and if none defined default to what is there.
;; (build {"cd /some/dir" "make bin"})
(fn build [tbl]
  (if (= tbl {})
      (print "No build defined.")
      (do
        (local sh (io.popen "/bin/sh" "w"))
        (sh:write (.. "cd " builddir "\n"))
        (each [k v (pairs tbl)]
          (do
            (sh:write (.. v "\n"))))
        (sh:close))))

;;(insert {"contents" "#!ipxe
;;dhcp
;;set syslogs 192.168.0.1
;;sync
;;chain http://192.168.0.20/install.ipxe"
;;          "dest" "src/chain.pxe"})
;;(insert {"file" "chain.ipxe"
;;         "dest" "src/chain.ipxe"})
(fn insert [tbl]
  "Include files as build time dependencies"
  (if (= tbl {})
      (print "No includes defined.")
      (do
        (each [k v (pairs tbl)]
          (do
            (var contents "")
            (let
                [file (. v "file")
                 dest (. v "dest")]
              (if (~= file nil)
                  (with-open [c (io.open file "r")]
                    (set contents (c:read "*a")))
                  (do
                    (set contents (. v "contents"))))
              (with-open [f (io.open (.. builddir "/" dest) "w")]
                (f:write contents))))))))

;;(inst 755 {"fennel"} bindr)
;;(inst 644 {"fennelbinary.lua" "fennelfriend.lua"} luashare)
(fn inst [tbl]
  "Install file/files with permissions to specified directory"
  (var installed "")
  (each [k v (pairs tbl)]
    (do
      (set installed "")
      (let
          [perms (. v "perms")
           out (. v "out")
           pkgs (. v 1)
           recurse (. v "recurse")]
        (if (= recurse true)
            (do
              (each [k v (pairs pkgs)]
                (do
                  (os.execute (.. "cd " builddir " && cp -r " v " " out))
                  (let
                      [i (io.popen (.. " ls -al " out))
                       ir (i:read "*a")]
                    (set installed (.. installed ir))))))
            (do
              (each [k v (pairs pkgs)]
                (do
                  (os.execute (.. "cd " builddir " && install -Dm" perms " " v " " out))
                  (let
                      [i (io.popen (.. "ls -al " out " | grep " v))
                       ir (i:read "*a")]
                    (set installed (.. installed ir)))))))
        (print (.. "The following has been installed to " out))
        (print installed)))))

(fn rename [tbl]
  (each [k v (pairs tbl)]
    (do
      (let
          [old (. v "old")
           new (. v "new")]
        (os.execute (.. "mv " builddir "/" old " " builddir "/" new))))))

(fn esperctrl [ctrl vargs]
  (if
   (= ctrl "fetch")
   (fetch vargs)
   (= ctrl "depends")
   (depends vargs)
   (= ctrl "insert")
   (insert vargs)
   (= ctrl "build")
   (build vargs)
   (= ctrl "rename")
   (rename vargs)
   (= ctrl "inst")
   (inst vargs)))

(fn init []
  (ensure ["~/bin"
           "~/.local/share/lua"
           "~/.local/share/lua/5.3"
           "~/.local/share/lua/5.2"
           "~/.local/share/lua/5.1"
           "~/.local/lib"]))

(fn usage []
  (print "Usage: esper pkg.esper
Author: Durrendal | GPLv3 | v0.1

-h, --help, help   -   Print this help message
init   -   Create common directories known to Esper"))

(fn main [esper]
  (if (= esper "init")
      (do
        (init)
        (os.exit)))
  (if
   (or (= esper "help") (or (= esper "--help")) (or (= esper "-h")) (or (= esper nil)))
   (do
     (usage)
     (os.exit)))
  (with-open
   [e (io.open esper :r)]
   (set espertbl (lume.deserialize (e:read "*a"))))
  (prepare)
  (if (~= (. espertbl "fetch") nil)
      (esperctrl "fetch" (. espertbl "fetch")))
  (if (~= (. espertbl "depends") nil)
      (esperctrl "depends" (. espertbl "depends")))
  (if (~= (. espertbl "insert") nil)
      (esperctrl "insert" (. espertbl "insert")))
  (if (~= (. espertbl "build") nil)
      (esperctrl "build" (. espertbl "build")))
  (if (~= (. espertbl "rename") nil)
      (esperctrl "rename" (. espertbl "rename")))
  (if (~= (. espertbl "inst") nil)
      (esperctrl "inst" (. espertbl "inst")))
  (cleanup))

(main ...)
