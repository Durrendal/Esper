#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
DESTDIR=/usr/bin

compile-lua:
	fennel --compile --require-as-include src/esper.fnl > src/esper.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/esper.lua
	sed -i '1 i\#!/usr/bin/env $(LUA)' src/esper.lua

install-lua:
	install ./src/esper.lua -D $(DESTDIR)/esper

compile-bin:
	cd ./src/ && fennel --compile-binary esper.fnl esper-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/esper-bin -D $(DESTDIR)/esper
