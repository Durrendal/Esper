# What?
An oversimplified packaging system made for tilde.town

# Overview:
![Esper Overview](.img/Esper-overview.gif)

Esper is meant to provide an easy to understand abstraction on packaging, while also providing a consumeable method for reproducing builds. On tilde.town users have ~/bin and ~/.local in their path, meaning they can lazily build personal tools and shareware, with an easy mechanism for localized installation. Esper helps to enable that localized tooling by making it easy to install programs, and for the less kowledgeable townies a complex and daunting series of build commands can be condensed to esper package.esper.

## Build Esper:
Esper can be built by running make compile-bin in the root directory of this repo. Additionally, Esper can be "bootstrapped" by running the bootstrap script in the root of this directory.

### Esper Bootstrap:
Assuming a system has lua, wget, & git installed Esper can be bootstrapped for you. The bootstrap script will build the localized directories Esper uses, and then pull and lume, lua5.3, and fennel and compile Esper for you to ~/bin.

(This process is still in development, a localized version of make and lua5.3 are still needed, as well as dependency checking)

## Esperbuild Symbols

### fetch:
```
fetch={url="https://some-url.com/dl/something.tar.gz.", git=false, outf="renamed-out-file", extract=true, atype="gzip"}
fetch={"url="https://gitlab.com/durrendal/esper.git", git=true}
fetch={at="/path/to/file.tar.gz", outf="something.tar.gz", extract=true, atype="gzip"}
fetch={at="/path/to/source"}

at - path on host to source files, or archive. if extrat = false copy to espersrc is recursive.
url - a valid HTTP/HTTPS url to an archive, or a git url
git - true or false, switches fetch to git handling if true
outf - file remapping, if the archive is named 0.4.2.tar.gz, but you want it to be fennel-0.4.2.tar.gz, sen outf="fennel-0.4.2.tar.gz (not needed for git urls, unless you're using a tagged release)
extract - true or false, denotes whether source should be extracted (not needed for git urls, unless you're using a tagged release)
atype - if extract is true, set this, provide it either gzip, bzip2, or zip (not needed unless you denote extract=true)
```

### depends:
```
depends={alpine="lua5.3 lua5.3-dev", debian="lua5.3 lua5.3-devel", redhat="lua lua-dev"}

alpine - a list of alpine packages, invokes apk add against list
debian - a list of debian packages, invokes apt-get install -y aginst list
redhat - a list of redhat packages, invokes yum install -y against list
```

### builddir:
```
builddir="/Sourcename/path/to/src/files"

Source is built in $(pwd)/espersrc by default, builddir appends an additional portions to the path.
builddir="fa/src" would be $(pwd)/espersrc/fa/src

a blank or absent builddir is equalt to $(pwd)/espersrc
```

### insert:
```
insert={file="chain.ipxe", dest="chain.ipxe"}

or

insert={contents=[["#!ipxe
dhcp
set syslogs 192.168.0.1
sync
chain http://192.168.0.20"]],
dest="chain.ipxe"}

Insert a build time dependency into the builddir, either by via an existing file, or by defining the contents of the dependent file inside of the esperbuild. If file is used it supercedes the contents argument, only one should be used at any given time. It's also worth noting that if you use contents you must encapsulate it in [[]] to denote a datatype of string inside the lua table.

This could technically be used for lazy patching files if desired by inserting an alternate into the source directory, or more properly invoking pathch in the build function with content blocks containing full patch files..
```

### build:
```
build={"make something"}

In its simplest form, build takes a table, which contains a series of commands that accomplish a software install. This can be extremely simple, or complex.

fa's build arg is {"make build"}
fennel's build arg is {"make fennel-bin"} but on Alpine we need additional args and it becomes {make fennel-bin LUA=lu5.3 STATIC_LUA_LIB=/usr/lib/liblua-5.3.so.0.0.0"}, both are fine.

We could even do something like:

{"make compile-lua",
 "chmod 777 something.lua",
 "sed -i 's/WORDS/NOT WORDS/g something.lua',
 "for file in $(ls); do echo $file; done"}

Go nuts, as long as you separate each command with a ' and quote it, we're good!
```

### rename:
```
rename={{old="src/something-bin", new="something"}}

old - path inside builddir to the binary you want renamed
new - name to be renamed to

(ex: esper compiles to esper-bin, but I want to install it as esper. I would provide {old="src/esper-bin", new="esper"})

providing multiple {old=x, new=y} tables in one rename call is supported.
```

### inst:
For files:
```
inst={{perms=777, {"some-file"} out="/some/directory"}}

inst is the install command, it takes one or more 3 key tables as arguments. Those tables have the following arguments

perms - set the permissions on the files in the table (such as 755 for executables, or 644 for libs)
{"some-file"} - the file or files to install (they must exist inside of the directory, if your builddir is fa/src then your some-file would need to be in fa/src/some-file. If it's in fa/src/exported then you would provide /exported/some-file
out - the directory to install all files in the table into (such as ~/bin for the tilde.town local bin path)
```

For Directories:
```
inst={{recurse=true, {"some-dir"}, out="/some/dir/"}}

inst can also be used to recursively install directories using the above schema. This was made specifically to deal with Java systems that provide tarballs as outputs, such that you can then extract, move, and then recurse to a directory. Midpoint's esper build is a prime example.

recurse - When set to true, it invokes a cp -r instead of an install -Dm
{"some-dir"} - a list of directories, or a directory, that you want to recursively copy
out - the directory to install the directory into
```

### debug:
```
debug=true

debug - true or false, if true, the espersrc directory is not removed after the build runs. If debug is not present it is assumed false
```

# Examples:

## Archive Builds:
```
{
   fetch={url="https://github.com/bakpakin/Fennel/archive/0.4.2.tar.gz",
          git=false,
          outf="fennel-0.4.2.tar.gz",
          extract=true,
          atype="gzip"},
   builddir="/Fennel-0.4.2",
   build={"make fennel-bin"},
   inst={
      {perms=755, {"fennel"}, out="~/bin/"},
      {perms=644, {"fennelfriend.lua", "fennelbinary.lua", "fennelview.lua", "fennel.lua"}, out="~/.local/share/lua/5.3/"}
   }
}
```

## Git Builds:
```
{
   fetch={url="https://notabug.org/m455/fa.git",
          git=true,
          outf=""},
   builddir="/fa/src",
   build={"make build"},
   inst={{perms=755, {"fa"}, out="~/bin/"}}
}
```

## Using Esper As An In Repo Source System:
Esper can be used as a semi-portable build/source system. Provided the lua compiled esperbuild with /usr/bin/env lua at the head of esper, you can place it and any correlating esperbuilds into any repo, and then source them through a makefile to provide necessary compilation dependencies. This is extremely useful in the event that you are using an edge language, like Fennel, which has not been packaged by major distributions (Debian, RedHat).

Fa currently uses Esper to provide a method to source Fennel, and thusly compile the program for distribution. For example this is the contents of the esperbuild directory in fa's repo, it contains a lua version of esper, and a fennel-dev.esper build script.

```
esperbuild[!]|>> ls -al
total 40
drwxr-sr-x 2 wsinatra wsinatra  4096 Sep 16 18:54 .
drwxr-sr-x 6 wsinatra wsinatra  4096 Sep 16 18:55 ..
-rw-r--r-- 1 wsinatra wsinatra 21246 Sep 16 18:51 esper
-rw-r--r-- 1 wsinatra wsinatra   186 Sep 16 18:53 fa.esper
-rw-r--r-- 1 wsinatra wsinatra   302 Sep 16 18:54 fennel-dev.esper
```

The makefile similarly offers to soure Fennel with. 
```
compile-fennel:
        cd ./esper && ./esper fennel.esper
```

The contents of fa.esper and fennel-dev.esper can be found in Esper's repo directory.