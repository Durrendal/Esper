* Source espers as deps
Source an existing esper as a dependency, and build it if necessary
* Change espersrc dir to tmp
Like what lcpaste does
* support multi espersrc dirs
* log installed espers
possibly .local/share/cache?
recfile db, or sqlite, either or?
A big old lua table could work too.
* Add dep checking
If an esper dep is fulfilled, don't compile/install
* Version tracking, with an always latest escape hatch
* Fix the installed X to Y thing, it is super flaky
* Rewrite in Nim/Go?
* Ini style esperbuild
[Build]
make compile
[Install]
tkts
[InstallTo]
.local/bin/

Something like this, Nim has really great parsing configuration for ini files, so it might be feasible. The format is flexible, but it should be pretty clear and self documenting if posible.
* Add a localized database
something that can go into like .config/esper/esper.db and track what was installed, when, and what version was installed.
* Add some sort of update functionality
* Add support for AUR style repo
esper.lambdacreate.space, more or less just a listing of esperbuilds that can be indexed and searched through
then pulled down to build at run time.
